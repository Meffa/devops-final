# DevOPS Final

Main app is ML app with  Gradient Boosting model trained on California housing dataset. Takes file with 8 comma-separeted features as input.  
Fallback app works without any ML model in it, just providing the mean value of input variables.

We can check correctness of applications by this commands and test.txt file provided:

`curl -L -F "file=@/vagrant/test/test.txt" predictor.my`
We should get response with report page and computed values

Then we can try to imitate main app failure by:
`kubectl scale deployment main-app`

And then again check if our fallback application takes its place:
`curl -L -F "file=@/vagrant/test/test.txt" predictor.my `

We should get report page with values others than the first time.
